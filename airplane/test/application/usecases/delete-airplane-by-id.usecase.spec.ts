import { mock, MockProxy } from 'jest-mock-extended';

import { AirplaneNotFoundError } from 'application/errors/airplane-not-found.error';
import { DeleteAirplaneByIdUseCase } from 'application/usecases/delete-airplane-by-id.usecase';
import { AirplaneRepositoryPort } from 'domain/port/airplane-repository.port';

describe('DeleteAirplaneByIdUseCase', () => {
  let airplaneRepositoryPort: MockProxy<AirplaneRepositoryPort>;
  let deleteAirplaneByIdUseCase: DeleteAirplaneByIdUseCase;

  beforeEach(async () => {
    airplaneRepositoryPort = mock();
    deleteAirplaneByIdUseCase = new DeleteAirplaneByIdUseCase(airplaneRepositoryPort);
  });

  describe('handler', () => {
    it('should throw error when airplane is not found', async () => {
      // GIVEN
      const uuid = '285deaf2-2871-11ed-a261-0242ac120002';

      // WHEN
      airplaneRepositoryPort.deleteAirplaneById.mockResolvedValue(false);

      // THEN
      await expect(deleteAirplaneByIdUseCase.handler(uuid)).rejects.toThrow(new AirplaneNotFoundError(uuid));
    });

    it('should delete airplane if it exists', async () => {
      // GIVEN
      const uuid = '3193bcf0-2871-11ed-a261-0242ac120002';

      // WHEN
      airplaneRepositoryPort.deleteAirplaneById.mockResolvedValue(true);

      // THEN
      await expect(deleteAirplaneByIdUseCase.handler(uuid)).resolves.not.toThrow();
    });
  });
});
