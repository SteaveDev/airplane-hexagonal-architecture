import { DynamicModule, Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

import InfrastructureModule from 'infrastructure/infrastructure.module';
import { typeOrmConfigAsync } from 'infrastructure/typeorm.config';

@Global()
@Module({})
export default class HandlerModule {
  static forRoot(setting: any): DynamicModule {
    return {
      module: HandlerModule,
      imports: [
        InfrastructureModule,
        TypeOrmModule.forRootAsync(typeOrmConfigAsync),
        ConfigModule.forRoot({
          ignoreEnvFile: process.env.NODE_ENV !== 'local',
          envFilePath: './config/env/.env.local',
          isGlobal: true,
        }),
      ],
      providers: [],
    };
  }
}
