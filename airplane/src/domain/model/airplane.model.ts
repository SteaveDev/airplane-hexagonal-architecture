import { DateTime } from 'luxon';

import { Color } from 'domain/model/enum/color.enum';

export interface Airplane {
  id: string;
  name: string;
  horsePower: number;
  color: Color;
  capacity: number;
  creationDate: DateTime;
}
