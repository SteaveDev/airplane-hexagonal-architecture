import { Airplane } from 'domain/model/airplane.model';

export interface AirplaneRepositoryPort {
  findAllAirplanes(name?: string): Promise<Airplane[]>;

  findAirplaneById(id: string): Promise<Airplane>; // | null

  saveAirplane(entity: Airplane): Promise<void>;

  deleteAirplaneById(id: string): Promise<boolean>;
}
