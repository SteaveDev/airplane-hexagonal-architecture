import { BadRequestException, INestApplication, ValidationError, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { NestFactory } from '@nestjs/core';

import HandlerModule from './handler.module';
import { JsonLoggerUtil } from 'infrastructure/utils/json-logger.util';

const logger = new JsonLoggerUtil('Handler');

function initNestConfig(nestApp: INestApplication) {
  nestApp.setGlobalPrefix('api');
  nestApp.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        logger.error('INIT_NEST_CONFIG', JSON.stringify(validationErrors));
        return new BadRequestException(validationErrors);
      },
    }),
  );

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Airplane')
    .setDescription('Airplane Access Service')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(nestApp, swaggerConfig);
  SwaggerModule.setup('/api/swagger', nestApp, document, {
    swaggerOptions: {
      supportedSubmitMethods: [],
    },
  });
}

export async function bootstrapLocal() {
  const nestApp = await NestFactory.create(HandlerModule.forRoot({}));

  initNestConfig(nestApp);

  await nestApp.listen(process.env.PORT || 3005);
}
