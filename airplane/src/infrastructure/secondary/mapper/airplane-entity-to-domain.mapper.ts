import { Airplane } from 'domain/model/airplane.model';
import { Color } from 'domain/model/enum/color.enum';

import { AirplaneEntity } from 'infrastructure/secondary/entity/airplane.entity';

export function airplaneEntityToDomain(entity: AirplaneEntity): Airplane {
  return {
    id: entity.id,
    name: entity.name,
    horsePower: entity.horsePower,
    color: Color[entity.color as keyof typeof Color],
    capacity: entity.capacity,
    creationDate: entity.creationDate,
  };
}
