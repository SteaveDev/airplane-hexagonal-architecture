export class DatabaseError extends Error {
  constructor() {
    super(`An error occurred while adding to database.`);
  }
}
