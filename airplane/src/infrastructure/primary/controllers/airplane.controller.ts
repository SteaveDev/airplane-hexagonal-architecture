import { Body, Controller, Delete, Get, HttpCode, Logger, NotFoundException, Param, Post, Query } from '@nestjs/common';

import { GetAllAirplanesUseCase } from 'application/usecases/get-all-airplanes.usecase';
import { GetAirplaneByIdUseCase } from 'application/usecases/get-airplane-by-id.usecase';
import { SaveAirplaneUseCase } from 'application/usecases/save-airplane.usecase';
import { DeleteAirplaneByIdUseCase } from 'application/usecases/delete-airplane-by-id.usecase';

import { CreateAirplaneDto } from 'application/dto/create-airplane.dto';
import { AirplaneNotFoundError } from 'application/errors/airplane-not-found.error';

import { GetAirplaneRequest } from 'infrastructure/primary/request/get-airplane.request';
import { DatabaseError } from 'infrastructure/utils/errors/database.error';
import { CreateAirplaneRequest } from 'infrastructure/primary/request/create-airplane.request';
import { QueryGetAllAirplanesRequest } from 'infrastructure/primary/request/query-get-all-airplanes.request';

@Controller('airplane')
export class AirplaneController {
  private readonly logger = new Logger(AirplaneController.name);

  constructor(
    private readonly getAllAirplanesUseCase: GetAllAirplanesUseCase,
    private readonly getAirplaneByIdUseCase: GetAirplaneByIdUseCase,
    private readonly saveAirplaneUseCase: SaveAirplaneUseCase,
    private readonly deleteAirplaneByIdUseCase: DeleteAirplaneByIdUseCase,
  ) {}

  @Get('/')
  public async findAll(@Query() query: QueryGetAllAirplanesRequest): Promise<GetAirplaneRequest[]> {
    this.logger.log(`[GET] Find all airplanes. name=${query.name}`);
    return this.getAllAirplanesUseCase.handler(query.name);
  }

  @Get('/:id')
  public async findById(@Param('id') id: string): Promise<GetAirplaneRequest> {
    this.logger.log(`[GET] Find all airplanes. id=${id}`);
    try {
      return await this.getAirplaneByIdUseCase.handler(id);
    } catch (e) {
      if (e instanceof AirplaneNotFoundError) {
        throw new NotFoundException(e.message);
      }
      throw e;
    }
  }

  @Post()
  public async save(@Body() entity: CreateAirplaneRequest): Promise<void> {
    this.logger.log(`[POST] Adding new airplane. airplane=${JSON.stringify(entity)}`);

    const airplaneRequest: CreateAirplaneDto = {
      name: entity.name,
      horsePower: entity.horsePower,
      color: entity.color,
      capacity: entity.capacity,
    };

    try {
      await this.saveAirplaneUseCase.handler(airplaneRequest);
    } catch (e) {
      if (e instanceof DatabaseError) {
        throw new NotFoundException(e.message);
      }
      throw e;
    }
  }

  // TODO Update

  @HttpCode(204)
  @Delete('/:id')
  public async deleteById(@Param('id') id: string): Promise<void> {
    this.logger.log(`[DELETE] Delete airplane by ID. id=${id}`);
    try {
      await this.deleteAirplaneByIdUseCase.handler(id);
    } catch (e) {
      if (e instanceof AirplaneNotFoundError) {
        throw new NotFoundException(e.message);
      }
      throw e;
    }
  }
}
