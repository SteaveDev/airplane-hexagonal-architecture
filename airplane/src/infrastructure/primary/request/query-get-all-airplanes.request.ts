import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class QueryGetAllAirplanesRequest {
  @IsOptional()
  @ApiProperty()
  name?: string;
}
