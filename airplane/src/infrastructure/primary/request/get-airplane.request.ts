import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsEnum, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';
import { DateTime } from 'luxon';

import { Color } from 'domain/model/enum/color.enum';

export class GetAirplaneRequest {
  @IsNotEmpty()
  @ApiProperty()
  id!: string;

  @IsNotEmpty()
  @ApiProperty()
  name!: string;

  @IsNotEmpty()
  @ApiProperty()
  horsePower!: number;

  @IsEnum(Color)
  color!: Color;

  @IsNotEmpty()
  @ApiProperty()
  capacity!: number;

  @IsNotEmpty()
  @IsDateString()
  @Type(() => String)
  @ApiProperty({
    description: `Creation date in ISO string format.<br>
         Examples:<br>
         2022-05-25T09:08:34.123<br>
         2016-W05-4`,
    type: 'string',
    format: 'date-time',
  })
  @Type(() => String)
  creationDate!: DateTime;
}
