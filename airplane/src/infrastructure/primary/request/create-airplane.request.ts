import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';

import { Color } from 'domain/model/enum/color.enum';

export class CreateAirplaneRequest {
  @IsNotEmpty()
  @ApiProperty()
  name!: string;

  @IsNotEmpty()
  @ApiProperty()
  horsePower!: number;

  @IsEnum(Color)
  color!: Color;

  @IsNotEmpty()
  @ApiProperty()
  capacity!: number;
}
