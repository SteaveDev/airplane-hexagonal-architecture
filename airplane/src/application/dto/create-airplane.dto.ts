import { Color } from 'domain/model/enum/color.enum';

export class CreateAirplaneDto {
  name!: string;
  horsePower!: number;
  color!: Color;
  capacity!: number;
}
