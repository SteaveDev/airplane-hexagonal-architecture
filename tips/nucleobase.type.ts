type Nucleobase = 'Adenine' | 'Guanine' | 'Cytosine' | 'Thymine';

enum NUCLEOBASE {
  A = 'ADENINE',
  G = 'GUANINE',
  C = 'CYTOSINE',
  T = 'THYMINE',
}

type Pair = [NUCLEOBASE.A, NUCLEOBASE.T] | [NUCLEOBASE.C, NUCLEOBASE.G];
